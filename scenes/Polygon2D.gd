extends Polygon2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var edges = 3
var radius = 100

# Called when the node enters the scene tree for the first time.
func _ready():
	self.polygon = get_edge_coordinates(edges)

func _input(ev):
	if ev is InputEventKey and ev.pressed:
		if ev.scancode == KEY_UP:
			edges += 1
			self.polygon = get_edge_coordinates(edges)
		if ev.scancode == KEY_DOWN:
			edges = max(3, edges - 1)
			self.polygon = get_edge_coordinates(edges)

func get_edge_coordinates(n):
	var x = 0
	var y = 0
	var coordinates = PoolVector2Array()
	
	for i in range(0, n):
		x = sin((2 * PI / n) * i) * radius
		y = cos((2 * PI / n) * i) * radius
		coordinates.append(Vector2(x,y))
	return coordinates

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
